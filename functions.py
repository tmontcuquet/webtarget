import os
import urllib.request
import smtplib
from tkinter import *

# def checkDoublonfromFile(cheminFichier):
#     with open(cheminFichier, "r") as f:
#         unique_lines = set(f.readlines())
#     with open(cheminFichier, "w") as f:
#         f.writelines((unique_lines))

def lireFichier(nomFichier):
    listemail=[]
    fichier = open(nomFichier, "r")
    listemail = fichier.readlines()
    fichier.close()
    return listemail

def ecrireFichier(nomFichier, contenu):
    fichier = open(nomFichier, "a")
    fichier.write(contenu + "\n")
    fichier.close()

def checkDoublon(liste):
    return list(set(liste))

def verifEmail(mail):
    if "@" in mail and (mail[-4:] == ".com" or mail[-3:] == ".fr"):
        print("Mail valide")
    else:
        print("Mail invalide")

def extraireDomaine(mail):
    domaine = mail.split('@')[1]
    return domaine

def pingDomaine(mail):
    ping = False
    domaine = mail[mail.find("@"):]
    domaine = domaine.replace("@", "")
    print(domaine)
    reponse = os.system("ping -t  www." + domaine)
    if reponse == 0:
        ping = True
    else:
        ping = False
    return ping

def crawler(url):
    site = urllib.request.urlopen(url).read()
    listeMails = re.findall("[\w\.-]+@[\w\.-]+", str(site))
    return listeMails

def envoiMail(mailenvoi, mdp, mailrecoit, msg):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(mailenvoi, mdp)
    server.sendmail(mailenvoi, mailrecoit, msg)
    server.quit()