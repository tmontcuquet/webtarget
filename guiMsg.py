from tkinter import *

fenetreMsg = Tk()

labelExpe = Label(fenetreMsg, text="Expediteur :", padx = 50, pady = 50, font=("Arial", 15))
labelExpe.pack()

valueExpe = StringVar()
valueExpe.set("")
entreeExpe = Entry(fenetreMsg, width = "30")
entreeExpe.pack()

labelObjet = Label(fenetreMsg, text="Objet :", padx = 50, pady = 50, font=("Arial", 15))
labelObjet.pack()

valueObjet = StringVar()
valueObjet.set("")
entreeObjet = Entry(fenetreMsg, width = "30")
entreeObjet.pack()

labelMsg = Label(fenetreMsg, text="Message :", padx = 50, pady = 50, font=("Arial", 15))
labelMsg.pack()

valueMsg = StringVar()
valueMsg.set("")
entreeMsg = Entry(fenetreMsg, width = "30")
entreeMsg.pack()

fenetreMsg.mainloop()