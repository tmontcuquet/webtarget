from tkinter import *
from guiMain import *

fenetreStart = Tk()

labelTitre = Label(fenetreStart, text="Nom Campagne", anchor = "center", padx = 50, pady = 50, font=("Arial", 20))
labelTitre.pack()

valueFichier= StringVar()
valueFichier.set("")
entreeFichier = Entry(fenetreStart, width = "30")
entreeFichier.pack()

boutonContinuer = Button(fenetreStart, text="Continuer", command = lambda: main())
boutonContinuer.pack()

fenetreStart.mainloop()
