from tkinter import *
def main():

    fenetreMain = Tk()

    boutonDoublon = Button(fenetreMain, text="Dédoublonner")
    boutonDoublon.pack()

    boutonImportCSV = Button(fenetreMain, text="Importer CSV")
    boutonImportCSV.pack()

    boutonMailist = Button(fenetreMain, text="Test mailist")
    boutonMailist.pack()

    boutonImportURL = Button(fenetreMain, text="Importer URL")
    boutonImportURL.pack()

    listboxMail = Listbox(fenetreMain)
    listboxMail.pack()

    boutonValid = Button(fenetreMain, text="Valider")
    boutonValid.pack()

    fenetreMain.mainloop()